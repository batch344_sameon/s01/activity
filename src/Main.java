import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = myObj.nextLine();

        System.out.println("Last Name: ");
        String lastName = myObj.nextLine();

        System.out.println("First Subject Grade: ");
        Double firstSubject = myObj.nextDouble();

        System.out.println("Second Subject Grade: ");
        Double secondSubject = myObj.nextDouble();

        System.out.println("Third Subject Grade: ");
        Double thirdSubject = myObj.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + ".");

        double average = (firstSubject + secondSubject + thirdSubject) / 3;
        System.out.println("Your grade average is: " + ((int)average));
    }
}